package org.weaverlandia.util;

import java.util.ArrayList;

import org.weaverlandia.data.Estadistica;
import org.weaverlandia.data.Resultado;

/**
 * 
 * @author weaver7x
 * @version 1.0
 *
 */
public class CalculaEstadisticas {
	
	/**
	 * Calcula las estadisticas sobre el primer jugador
	 * @param resul
	 * @return
	 */
	public final static Estadistica calculoEstadisticaUno(ArrayList<Resultado> resul) {
		
		int estadisticas[] = new int[6];
		for (Resultado r: resul) {
			
			if (r.getResul1() < r.getResul2()) {
				estadisticas[1]++; //derrota 
			} else if (r.getResul1() > r.getResul2()) {
				estadisticas[0]++; //victoria
			} else {
				estadisticas[2]++; //empate
			}
			
			estadisticas[3] += r.getResul1(); //GF
			estadisticas[4] += r.getResul2(); //GC
					
		}
		
		estadisticas[5]	= estadisticas[3] - estadisticas[4]; // Diferencia de goles
		final Estadistica esta = new Estadistica(estadisticas);
		return esta;
	}
	
	/**
	 * Calcula las estadisticas sobre el segundo jugador a traves del primero
	 * @param es1
	 * @return
	 */
	public final static Estadistica calculoEstadisticaDos(Estadistica es1) {
		int estadisticas[] = new int[6];
		
		estadisticas[0] = es1.getDerrotas(); // Victorias
		estadisticas[1] = es1.getVictorias();
		estadisticas[2] = es1.getEmpates();
		estadisticas[3] = es1.getGolesContra(); // Favor
		estadisticas[4] = es1.getGolesFavor();
		estadisticas[5] = estadisticas[3] - estadisticas[4];
		
		final Estadistica esta = new Estadistica(estadisticas);
		return esta;
	}

}