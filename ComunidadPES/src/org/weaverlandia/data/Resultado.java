/**
 * 
 */
package org.weaverlandia.data;

/**
 * Nota, solo valen resultados de una cifra.
 * @author weaver7x
 * @version 1.0
 *
 */
public class Resultado {

	protected int resul1;
	
	protected int resul2;
	
	public Resultado(int r1, int r2) {
		this.resul1 = r1;
		this.resul2 = r2;
	}
	
	public final int getResul1(){
		return this.resul1;
	}
	
	public final int getResul2(){
		return this.resul2;
	}
	
	public final void setResul1(int res){
		this.resul1 = res;
	}
	
	public final void setResul2(int res){
		this.resul2 = res;
	}

}
