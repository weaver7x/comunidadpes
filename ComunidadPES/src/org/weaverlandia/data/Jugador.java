package org.weaverlandia.data;

/**
 * 
 * @author weaver7x
 * @version 1.0
 *
 */
public class Jugador {

	protected String nombre;
	
	protected Estadistica esta;
	
	public Jugador() {
		this.nombre = "Jugador";
		this.esta = new Estadistica();
	}
	
	public Jugador (String n, Estadistica e) {
		this.nombre = n;
		this.esta = e;
	}
	
	public void setNombre(String n) {
		this.nombre = n;
	}
	
	public void setEstadistica(Estadistica e) {
		this.esta = e;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Estadistica getEstadistica() {
		return this.esta;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.nombre);
		sb.append(": ");
		sb.append(this.esta.victorias);
		sb.append(" [G] ");
		sb.append(this.esta.derrotas);
		sb.append(" [P] ");
		sb.append(this.esta.empates);
		sb.append(" [E] ");
		sb.append(this.esta.golesFavor);
		sb.append(" [GF] ");
		sb.append(this.esta.golesContra);
		sb.append(" [GC] ");
		sb.append(this.esta.dif);
		sb.append(" [D] ");
		return sb.toString();
	}
		
}
