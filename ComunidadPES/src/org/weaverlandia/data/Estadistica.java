package org.weaverlandia.data;

/**
 * 
 * @author weaver7x
 * @version 1.0
 *
 */
public class Estadistica {
	
	protected int victorias;
	
	protected int derrotas;
	
	protected int empates;
	
	protected int golesFavor;
	
	protected int golesContra;
	
	protected int dif;
	
	public Estadistica() {
		this.victorias = 0;
		this.derrotas = 0;
		this.empates = 0;
		this.golesFavor = 0;
		this.golesContra = 0;
		this.dif = 0;	
	}
			
	public Estadistica(int v, int d, int e, int gf, int gc, int diferencia) {
		this.victorias = v;
		this.derrotas = d;
		this.empates = e;
		this.golesFavor = gf;
		this.golesContra = gc;
		this.dif = diferencia;
	}
	
	public Estadistica(int[] i) {
		this.victorias = i[0];
		this.derrotas = i[1];
		this.empates = i[2];
		this.golesFavor = i[3];
		this.golesContra = i[4];
		this.dif = i[5];
	}
	
	public final int getVictorias(){
		return this.victorias;
	}
	
	public final int getDerrotas(){
		return this.derrotas;
	}
	
	public final int getEmpates(){
		return this.empates;
	}
	
	public final int getGolesFavor(){
		return this.golesFavor;
	}
	
	public final int getGolesContra(){
		return this.golesContra;
	}
	
	public final int getDif(){
		return this.dif;
	}
	
	public final void setVictorias(int param){
		this.victorias = param;
	}
	
	public final void setDerrotas(int param){
		this.derrotas = param;
	}
	
	public final void setEmpates(int param){
		this.empates = param;
	}
	
	public final void setGolesFavor(int param){
		this.golesFavor = param;
	}
	
	public final void setGolesContra(int param){
		this.golesContra = param;
	}
	
	public final void setDif(int param){
		this.dif = param;
	}
}
